var app = angular.module("app",[]);
app.controller("TestController", function ($scope, $http) {
   
    $scope.language='es';
    $scope.slides = [];
    $scope.createcarousel =false;
    $scope.index =0;

    // Update text
    $scope.Update = function () {
          
        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;',
                'accept-language': $scope.language
            }
        }

        var  url = 'https://staging-api.doc.com/api/catalog/text/app/home';
        $http.get(url, config)
        .then(function (response) {
            console.log("resp");
            if(response.data.success==true){
                
                if($scope.createcarousel==false){
                    $scope.slides = response.data.data.messages;
                    $scope.createcarousel=true;
                }else{
                    var sl=response.data.data.messages;
                    var length = sl.length;
                    for(var i=0; i< length; i++){
                        $scope.slides[i].text =sl[i].text;
                        $scope.slides[i].timeout =sl[i].timeout;
                    }
                }
            }

        },function (data, status, header, config) {
            console.log( "Data: " + data +
                " status: " + status +
                " headers: " + header +
                " config: " + config);
        }); 
    };


    $scope.finished=function (){
        $(function () {
            // Crerate carousel
            $scope.owl=$(".owl-carousel").owlCarousel({
                loop:true,
                margin:0,
                responsiveClass:true,
                navigation: false,
                pagination: false,
                dots: false,
                items:1,
                autoHeight:true,
                freeDrag:false,
                touchDrag:false,
                mouseDrag:false,
                autoplay:false
            });
            
            // set timeout for slide
            $scope.setDelay=function(){
                
                var timeout = 0;
                if( $scope.index>=$scope.slides.length){
                    $scope.index=0;
                }
                timeout = $scope.slides[$scope.index].timeout;
                $scope.index++;
                setTimeout(function () {
                    $scope.owl.trigger('next.owl.carousel'); 
                    $scope.setDelay();
                },timeout);
            }   

            // Init
            $scope.setDelay();
        });

    }

    $scope.Update();

});

