const express = require('express');
const path = require('path');


// Loader Routes
const indexRouter = require('./routes/test1/index');
const usersRouter = require('./routes/test1/user');
const customerRouter = require('./routes/test1/customer');
const adminRouter = require('./routes/test1/administrator');
const testsRouter = require('./routes/test2/tests');

var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Api path
const base = '/api/v1';

// ROUTERS

// TEST 1

// Pages 
app.use('/', indexRouter);

// Roles 
app.use(base+'/', usersRouter);
app.use(base+'/customer', customerRouter);
app.use(base+'/administrator', adminRouter);


// Tests 2 Exercices
app.use(base+'/tests', testsRouter);

// static
app.use('/public', express.static(__dirname + '/public'));


// Error 404
app.use(function(req, res, next) {
   // render the error page
   res.status(404);
   res.json({ success:false,message: "Ruta no encontrada" });
});

// Error handler
app.use(function(err, req, res, next) {
 
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json({ success:false,message: err.message, error: err });
});

app.set('port', process.env.PORT || 3000);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});


