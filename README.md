## Documentation API TEST

## Installation

```console
npm install
```

## Base Routes 

{{api}} => http://localhost:3000/api/v1

{{url}} => http://localhost:3000/


## TEST 1

### User test

Adminsitrator
```json
{
    "username":"admin",
    "password":"adm123",
}
```

Customer
```json
{
    "username":"customer",
    "password":"cus123"
}
```

### Login

**GET** {{api}}/login

Send

```json
{
	"username":"admin",
	"password":"adm123"
}
```

Response

```json
{
    "auth": true,
    "token": ""
}
```

### Recovery Password

**GET** {{api}}/recoverypassword

Send

```json
{
	"email":"customer@gmail.com"
}
```

Response

```json
{
    "message": "Se enviara un correo con la contraseña (no sirve XD)"
}
```


### Administrator

#### profile

**GET** {{api}}/administrator/profile

Response

```json
{
    "id": 1,
    "username": "admin",
    "role": "ADMINISTRATOR",
    "name": "Un administrador",
    "email": "admin@gmail.com"
}
```

#### products 

**GET** {{api}}/administrator/products
**GET** {{api}}/administrator/products/:category

Response

```json
{
    "success": true,
    "products": [
        {
            "sku": "cpainvi",
            "name": "Capa de invisibilidad",
            "description": "Capa que otorga invisivilidad al usuarla",
            "price": 2000,
            "image": "capa.jpg",
            "category": "ropa"
        }
    ]
}
```

#### Edit product 

**POST** {{api}}/administrator/product/:sku

Send
```json
{
	"name":"Capa de invisibilidad",
    "description":"Capa que otorga invisivilidad al usuarla"
}
```

#### Delete product

**DELETE** {{api}}/administrator/product/:sku


### Customer

#### profile

**GET** {{api}}/customer/profile

#### products 

**GET** {{api}}/customer/products
**GET** {{api}}/customer/products/:category


### Free Access

#### Index
**GET** {{url}}/

#### Terms
**GET** {{url}}/terms

#### PDF
**GET** {{url}}/pdf


## TEST 2

#### Excercise 1
**GET** {{url}}/exercise1

Response html

### Excercise 2 
**GET** {{api}}/tests/exercise2

Send
```json
datei:2018-06-01
datef:2018-12-01
```

Response

```json
{
    "days": 1
}
```

### Excercise 3 
**GET** {{api}}/tests/exercise3

Send

```json
yeari:2000
yearf:2018
```

Response

```json
{
    "_str": "2000@2004@2008@2012@2016",
    "_array": [
        2000,
        2004,
        2008,
        2012,
        2016
    ]
}
```


### Excercise 4 
**POST** {{api}}/tests/exercise4

Send

```json
{
	"n":2,
	"rot":[0,-2],
	"coord":[0,1]
}
```

Response

```json
{
    "value": 3,
    "matrix": [
        [
            4,
            3
        ],
        [
            2,
            1
        ]
    ]
}
```

### Excercise 5 
**POST** {{api}}/tests/exercise5

Send

```json
{
	"s":"LOREM ipsum la gata pinta esta aqui"
}
```

Response

```json
{
    "cadena": {
        "l": 2,
        "o": 1,
        "r": 1,
        "e": 2,
        "m": 2,
        "i": 3,
        "p": 2,
        "s": 2,
        "u": 2,
        "a": 6,
        "g": 1,
        "t": 3,
        "n": 1,
        "q": 1
    }
}
```


### Excercise 6 
**POST** {{api}}/tests/exercise6

Send
```json
{
	"d":"./routes/test1/libs"
}
```

Response

```json
{
    "path": "./routes/test1/libs",
    "children": [
        {
            "path": "./routes/test1/libs/Auth.js"
        },
        {
            "path": "./routes/test1/libs/config.json"
        },
        {
            "path": "./routes/test1/libs/mockdata.json"
        }
    ]
}
```


### Excercise 7 

Only return number routes

**GET** {{api}}/tests/exercise7

Send

```json
n:2
```

Response

```json
{
    "num_routes": 6
}
```

### Excercise 7 A

Display all routes

**GET** {{api}}/tests/exercise7

Send

```json
n:2
```

Response

```json
{
    "num_routes": 6,
    "routes": [
        "AADD",
        "ADAD",
        "ADDA",
        "DAAD",
        "DADA",
        "DDAA"
    ]
}
```