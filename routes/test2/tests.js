var express = require('express');
var router = express.Router();

// Loader Excercices 
const exercise2 = require('./exercises/exercise2');
const exercise3 = require('./exercises/exercise3');
const exercise4 = require('./exercises/exercise4');
const exercise5 = require('./exercises/exercise5');
const exercise6 = require('./exercises/exercise6');

const exercise7 = require('./exercises/exercise7');
const exercise7a = require('./exercises/exercise7a');

// Routes
router.get('/exercise2',exercise2);
router.get('/exercise3',exercise3);
router.post('/exercise4',exercise4);
router.post('/exercise5',exercise5);
router.post('/exercise6',exercise6);


router.get('/exercise7',exercise7);
router.get('/exercise7a',exercise7a);

module.exports = router;
