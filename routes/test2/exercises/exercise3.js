/*
Dados dos parametros de entrada
• parametro 1: año inicio
• parametro 2: ano fin
El programa debe regresar un arreglo con los años que fueron bisiestos
La salida debe ser una cadena con los años separados por el caracter “@”
*/

const moment = require('moment'); 

function exercise3 (req, res, next) {
  var num_days = 0;

  // Exist Data
  if ( !req.query.yeari || !req.query.yearf ){
    res.status(400);
    res.json({
      message:"Faltan datos que ingresar"
    });
  }

  // Format year  
  const reg =/\d\d\d\d/;

  if ( req.query.yeari.match(reg) ===null ||
    req.query.yearf.match(reg) ===null ){

    res.status(400);
    res.json({
      message:"Los datos no tienen formato de año YYYY"
    });
  }

    // Find leapyear 
    var isleapyear=false;
    var start = parseInt(req.query.yeari);
    var end = parseInt(req.query.yearf);

    while(isleapyear===false){
        isleapyear=moment([start]).isLeapYear();
        if(isleapyear==false){
            start++;
        }
    }
    
    // Set and add 4 years
    var curyear = start;
    var ayears =[curyear];
    var bstop = false;
    while(bstop==false){
        curyear+=4;
        if(curyear<=end){
            ayears.push(curyear);
        }else{
            bstop=true;
        }
    }
    
    // String  
    var syears = ayears.join("@");

    res.json({
        _str:syears,
        _array:ayears
    });

}

module.exports = exercise3;
