/*
Otra Solucion visible
Si se inicia en la esquina superior izquierda de un grid de 2x2, y las unicos posibles
movimientos son “derecha” y “abajo”. Existen exactamente 6 rutas para llegar a la esquina
inferior derecha,
*/

function permut(string) {
  if (string.length < 2) return string; // This is our break condition

  var permutations = []; // This array will hold our permutations

  for (var i=0; i<string.length; i++) {
      var char = string[i];

      // Cause we don't want any duplicates:
      if (string.indexOf(char) != i) // if char was used already
          continue;           // skip it this time

      var remainingString = string.slice(0,i) + string.slice(i+1,string.length); //Note: you can concat Strings via '+' in JS

      for (var subPermutation of permut(remainingString))
          permutations.push(char + subPermutation)

  }
  return permutations;
}


function exercise7a (req, res, next) {
 
    // Exist Data
    if ( !req.query.n  ){
         res.status(400);
         res.json({
           message:"Faltan datos que ingresar"
         });
    }

    var regexp = /^\+?(0|[1-9]\d*)$/;
    if ( !(req.query.n.match(regexp)) || (req.query.n<2) ){
        res.status(400);
        res.json({
          message:"El parametro debe ser un número entero mayor a 1"
        });
   }

   var movedown ="";
   var moveright="";
   var n = req.query.n;

   // The necessary movements are calculated in strings
   // A = abajo
   // D = derecha

   for (var i=1 ; i<= req.query.n; i++){
      movedown+="A";
      moveright+="D";
   }

   var string = movedown+moveright;
   // All possible combinations are calculated
   var permutations = permut(string);
  
   res.json({
     num_routes:permutations.length,
     routes:permutations
   });
 
 }
 
 module.exports = exercise7a;
 