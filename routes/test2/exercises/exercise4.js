/*
Dados los parametros de entrada:
• n (int) = tamaño de la matriz(nxn)
• rotaciones[int] = un arreglo que contenga una sucesión de rotaciones representadas
por números, los números positivos son rotaciones a la derecha y los números
negativos son rotaciones a la izquierda (0 representa ningun movimiento).
• coordenada [x, y] = La coordenada del número de la salida.
Se genera una matriz “n”x”n” y se llena con números comenzando por el 1 en la celda 0,0 y
aumentando en 1 consecuentemente en cada celda de la matriz, aplicar las rotaciones
necesarias y posterior a la rotaciones a la mtariz y dada la coordenada[x,y] se tiene que
regresar el número de esa celda.
La salida será el número contenido en la coordenada indicada.
*/

// Matrix rotate left
function rotLeft(a){
    var n=a.length;
    for (var i=0; i<n/2; i++) {
        for (var j=i; j<n-i-1; j++) {
            var tmp=a[i][j];
            a[i][j]=a[j][n-i-1];
            a[j][n-i-1]=a[n-i-1][n-j-1];
            a[n-i-1][n-j-1]=a[n-j-1][i];
            a[n-j-1][i]=tmp;
        }
    }

    console.log(a);
}

// Matrix rotate right
function rotRight(a) {
    var n=a.length;
    for (var i=0; i<n/2; i++) {
        for (var j=i; j<n-i-1; j++) {
            var tmp=a[i][j];
            a[i][j]=a[n-j-1][i];
            a[n-j-1][i]=a[n-i-1][n-j-1];
            a[n-i-1][n-j-1]=a[j][n-i-1];
            a[j][n-i-1]=tmp;
        }
    }
    console.log(a);
}


function rotate (matrix,dir){

    console.log("Begin rotate dir :"+dir);
    console.log(matrix);
    console.log("Rotations:");
    var times = Math.abs(dir);

    if(dir>0){
        for(var i=1 ; i<=times ;i++){
            rotRight(matrix);
        }
    }else if(dir<0){
        for(var i=1 ; i<=times ;i++){
            rotLeft(matrix);
        }
    }

}

function exercise4 (req, res, next) {

    // Exist Data
    if ( !req.body.n || !req.body.rot || !req.body.coord  ){
        res.status(400);
        res.json({
        message:"Faltan datos que ingresar"
        });
    }


    // Format data
    if ( !(Number.isInteger(req.body.n)) || (req.query.n<2)  ){
        res.status(400);
        res.json({
        message:"El parametro debe ser un número entero mayor a 1"
        });
    }

    // Array number elements need
    if( !(req.body.rot.length>0) || !(req.body.coord.length==2) ){
        res.status(400);
        res.json({
        message:"Las coordenadas deben tener 2 enteros y/o las rotaciones minimo 1 entero"
        });
    }

    // Array format 
    if( req.body.rot.some(isNaN) || req.body.coord.some(isNaN) ){
        res.status(400);
        res.json({
        message:"Las coordenadas y/o rotaciones deben ser arreglos de numeros enteros"
        });
    }

    // Set data
    var n = req.body.n;
    var rot =req.body.rot;
    var coord = req.body.coord;

    // Exist coords ?
    if( (coord[0]>(n-1)) || (coord[1]>(n-1))  ){
        res.status(400);
        res.json({
        message:"Las coordenadas ("+coord[0]+","+coord[1]+") no existen en una matriz de "+n+" X "+n
        });
    }

    // Fill matrix
    var matrix= new Array(n);
    var count=1;

    for (var i = 0; i < n; i++) {
        matrix[i]=new Array(n);
        for (var j = 0; j < n; j++) {
            matrix[i][j] = count;
            count++;
        }
    }

    // Each rotate 
    var lrot = rot.length;
    for (var j =0 ; j < lrot ; j++){  
        rotate(matrix,rot[j]);
    }
   
    res.json({
        value:matrix[coord[0]][coord[1]],
        matrix:matrix
    });

}

module.exports = exercise4;
