/*
Solucion Solicitada
Si se inicia en la esquina superior izquierda de un grid de 2x2, y las unicos posibles
movimientos son “derecha” y “abajo”. Existen exactamente 6 rutas para llegar a la esquina
inferior derecha.

La solucion es:
El número de combinaciones posibles de N x N caminos en K movimientos en una sola direccion 
*/

function factorial (n) { 
	if (n == 0){ 
		return 1; 
	}
	return n * factorial (n-1); 
}


function exercise7 (req, res, next) {
 
    // Exist Data
    if ( !req.query.n  ){
         res.status(400);
         res.json({
           message:"Faltan datos que ingresar"
         });
    }
    
    var regexp = /^\+?(0|[1-9]\d*)$/;
    if ( !(req.query.n.match(regexp)) || (req.query.n<2) ){
        res.status(400);
        res.json({
          message:"El parametro debe ser un número entero mayor a 1"
        });
    }

   // Calculate combinations
    var n = req.query.n*2;
    var k = req.query.n;
    var c = factorial(n)/( factorial(k)* ( factorial(n-k) ) );

    res.json({
        num_routes:c
    });
 
 }
 
 module.exports = exercise7;
 