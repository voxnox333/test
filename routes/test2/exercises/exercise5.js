/*
Dado los parametros de entrada:
• s (String) = Un texto de n caracteres (Lorem ipmsum…)
Obtener el numero de veces que se repite cada letra de la cadena
La salida sera una lista indicando el caracter con el numero de veces encontrada en la
cadena ( a = 15 , b = 6, c …)
*/

function exercise5 (req, res, next) {
 
   // Exist Data
   if ( !req.body.s  ){
        res.status(400);
        res.json({
        message:"Faltan datos que ingresar"
        });
    }

    var text = req.body.s;
    var chars={};
    const regexp = /[a-z]+/g;

    for ( var i=0 ;  i<=text.length; i++ ){
        
        var char = text.charAt(i).toLowerCase();
    
        if ( char.match(regexp)!=null ) {
            if ( chars.hasOwnProperty(char) ){
                chars[char]++;
            }else{
                chars[char]=1;
            }
        }        
    }

    res.json({
        cadena:chars
    });

}

module.exports = exercise5;
