/*
Dados dos parametros de entrada
• parametro 1: fecha inicio
• parametro 2: fecha fin
El programa debe regresar la cantidad de dias en el que el dia “Domingo” es el ultimo dia del
mes en ese lapso de tiempo
La salida debe ser el numero de dias
*/

const moment = require('moment'); 

function exercise2 (req, res, next) {
  var num_days = 0;

  // Exist Data
  if ( !req.query.datei || !req.query.datef ){
    res.status(400);
    res.json({
      message:"Faltan datos que ingresar"
    });
  }

  // Format Date  
  const reg =/\d\d\d\d-(0[1-9]|1[0-2])-(0[1-9]|[1-2]\d|3[0-1])/;

  if ( req.query.datei.match(reg) ===null ||
    req.query.datef.match(reg) ===null ){

    res.status(400);
    res.json({
      message:"Los datos no tienen formato de fecha"
    });
  }

  var startDate = moment(req.query.datei, 'YYYY-MM-DD');
  var endDate = moment(req.query.datef, 'YYYY-MM-DD');

  var time = moment(req.query.datei, 'YYYY-MM-DD');
  var bnext = true;

  while(bnext){
    var mendof=time.endOf('month');
    
    console.log(mendof.format('YYYY-MM-DD')+"  "+mendof.format('dddd'));
    
    if( startDate <= time && time <= endDate  ){
      if(mendof.format('e')==6){
        num_days++;
      }
      time = time.add(1, 'months');
    }else{
      bnext=false;
    }
  }
    
  res.json({
    days:num_days
  });

}

module.exports = exercise2;
