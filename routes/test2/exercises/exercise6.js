/*
Dado los parametros de entrada:
• d (String) = Una ruta al sistema de archivos
El programa debera listar todos los archivos de la ruta asi como los archivos de cada
subcarpeta hasta el ultimo nodo.
*/

const  fs = require('fs');
const  path = require('path');

function dirTree(filename) {
    var stats = fs.lstatSync(filename),
        info = {
            path: filename
        };
 
    if (stats.isDirectory()) {
        info.children = fs.readdirSync(filename).map(function(child) {
            return dirTree(filename + '/' + child);
        });
    }
 
    return info;
}

function exercise6 (req, res, next) {
 
    // Exist Data
    if ( !req.body.d  ){
         res.status(400);
         res.json({
           message:"Faltan datos que ingresar"
         });
    }
    
    
    // Validate exist directory
    if (!fs.existsSync(req.body.d)) {
        res.status(400);
         res.json({
           message:"No existe el directorio"
         });
    }
    
    const tree = dirTree(req.body.d);
    res.json(tree);
 
 }
 
 module.exports = exercise6;
 