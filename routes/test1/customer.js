const express = require('express');
const router = express.Router();

// Authorization
const Auth = require('./libs/Auth');
const role = 'CUSTOMER';
const valid = [Auth.verifytoken,Auth.checkrole(role)];

const Products = require('./libs/Products');

router.get('/profile',valid,(req, res, next)=>{
  var resp = Auth.getprofile(req.userAuth.id);
  res.json(resp);
});

// All products 
router.get('/products',valid,(req, res, next)=>{
  var products = Products.get();
  res.json(products);
});

// Filter by categories
router.get('/products/:category',valid,(req, res, next)=>{
  var products = Products.get(req.params.category);
  res.json(products);
});

module.exports = router;
