const express = require('express');
const router = express.Router();

// Authorization
const Auth = require('./libs/Auth');
const role = 'ADMINISTRATOR';
const valid = [Auth.verifytoken,Auth.checkrole(role)];

const Products = require('./libs/Products');

router.get('/profile',valid,(req, res, next)=>{
  var resp = Auth.getprofile(req.userAuth.id);
  res.json(resp);
});

// All products 
router.get('/products',valid,(req, res, next)=>{
  var products = Products.getall();
  res.json(products);
});

// Filter by categories
router.get('/products/:category',valid,(req, res, next)=>{
  var products = Products.getall(req.params.category);
  res.json(products);
});

// Edit product
router.post('/product/:sku',valid,(req, res, next)=>{


  
  var data = {
    name : req.body.name,
    description:req.body.description
  }

  var products = Products.edit(req.params.sku,data);
  res.json(products);
});

// Delete product
router.delete('/product/:sku',valid,(req, res, next)=>{
  var products = Products.delete(req.params.sku);
  res.json(products);
});

module.exports = router;
