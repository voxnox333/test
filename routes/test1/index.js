const express = require('express');
const router = express.Router();
var path = require('path');

router.get('/',(req, res, next)=>{
  res.sendFile(path.resolve(__dirname+'/../../views/index.html'));
});

router.get('/pdf',(req, res, next)=>{
  res.sendFile(path.resolve(__dirname+'/../../docs/test.pdf'));
});

router.get('/terms',(req, res, next)=>{
  res.sendFile(path.resolve(__dirname+'/../../views/terms.html'));
});

// Exercise 1
router.get('/exercise1',(req, res, next)=>{
  res.sendFile(path.resolve(__dirname+'/../../views/sliders.html'));
});

module.exports = router;