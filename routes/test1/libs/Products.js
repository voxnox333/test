const products = require('./../resources/products');

// Products
class Products {
    
    //Get all products 
    getall(category=null){
        
        if(category!==null){
            var strcategory = category.toLowerCase();
            var filter = products.filter( product => (product.category === strcategory) );
            
            if(filter.length==0){
                return {
                success:false,
                message:"No hay productos de la categoría "+category
                };
            }

            return {
                success:true,
                products:filter
            };
        }else{
            return {
                success:true,
                products:products
            };
        }

    }

    // valid exist
    exist(sku){
        var product = products.filter( product => (product.sku === sku) );
        return product;
    }

    // edit product
    edit(sku, data){
        
        var product = this.exist(sku);

        if(product==null){
            return {
                success:false,
                message:"No exite el producto con sku "+sku
            }
        }

        return {
            success:true,
            message:"Se edito correctamente"
        }
    }


    delete(sku){
        var product = this.exist(sku);

        if(product==null){
            return {
                success:false,
                message:"No exite el producto con sku "+sku
            }
        }

        return {
            success:true,
            message:"Se elimino correctamente"
        }
    }
}

module.exports = new Products();