const jwt = require('jsonwebtoken');
const config = require('./../resources/config');
const users = require('./../resources/users');

// Auth
class Auth {
    
    // constructor
    constructor() {

    }

    // Validate token 
    verifytoken(req,res,next){

        const bearerHeader = req.headers['authorization'];
    
        if (typeof bearerHeader!== 'undefined'){
    
            const bearer = bearerHeader.split(' ');
            const bearerToken = bearer[1];
        
            jwt.verify(bearerToken, config.secret, function(err, decoded) {
                if (err)
                return res.status(500).json({ 
                        auth: false, 
                        message: 'Fallo la autenticación.' 
                    });
    
                req.userAuth = decoded;
                console.log(req.userAuth);
                next();
            });
    
        }else{
            res.status(401).json({
                auth:false,
                message:"Acceso no autorizado"
            });
        }
    
    }

    // Role access
    checkrole(role){
        return (req,res,next) => { 
                if(req.userAuth.role===role){
                    next();
                }else{
                    res.status(403).send({
                        auth:false,
                        message:"No tienes permitido acceder a esta locación"
                    });
                }
        }
    }

    // Get profile
    getprofile(id){
        var user = users.find( user => (user.id === id) );
    
        return {
            id:user.id,
            username:user.username,
            role:user.role,
            name:user.name,
            email:user.email
        };
    }
}

module.exports = new Auth();