module.exports=[
    {
        sku:"cpainvi",
        name:"Capa de invisibilidad",
        description:"Capa que otorga invisivilidad al usuarla",
        price:2000,
        image:"capa.jpg",
        category:"ropa"
    },
    {
        sku:"sombmerl",
        name:"Sombrero de Mago",
        description:"Sombrero hecho de piel de dragon y adornado de joyas",
        price:7000,
        image:"sombremerl.jpg",
        category:"ropa"
    },
    {
        sku:"tunicaelf",
        name:"Túnica élfica",
        description:"Túnica elaborada por elfos",
        price:12000,
        image:"tunicaelf.jpg",
        category:"ropa"
    },
    {
        sku:"swordmth",
        name:"Espada de mitrilo",
        description:"Espada hecha con mitrilo",
        price:50000,
        image:"swordmth.jpg",
        category:"armas"
    },
    {
        sku:"bastmoon",
        name:"Bastón lunar",
        description:"Bastón de cedro , bendecido por la luz de la luna",
        price:12000,
        image:"bastmoon.jpg",
        category:"armas"
    }
]