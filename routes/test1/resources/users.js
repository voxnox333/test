module.exports=[
    {
        id:1,
        username:"admin",
        password:"adm123",
        role:"ADMINISTRATOR",
        name:"Un administrador",
        email:"admin@gmail.com"
    },
    {
        id:2,
        username:"customer",
        password:"cus123",
        role:"CUSTOMER",
        name:"Un cliente",
        email:"customer@gmail.com"
    },
    {
        id:3,
        username:"staff",
        password:"staff123",
        role:"STAFF",
        name:"Un personal",
        email:"staff@gmail.com"
    }
];
