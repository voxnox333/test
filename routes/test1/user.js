const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

//Data
const users = require('./resources/users');
const config = require('./resources/config');

// Login
router.post('/login', function(req, res, next) {
  
  // Exist Data
  if ( !req.body.username || !req.body.password ){
    res.status(400);
    res.json({
      message:"Faltan datos que ingresar"
    });
  }

  // Authorization
  var user = users.find( user => (user.username === req.body.username && user.password === req.body.password ) );

  if(!user){
    res.status(401);
    res.json({
      auth: false,
      message:"El usuario y/o la contraseña son incorrectos"
    });
  }

  // Generate token
  var token = jwt.sign({ id: user.id , role:user.role, name:user.name }, config.secret, {
    expiresIn: "24h"
  });

  res.json({
      auth: true,
      token: token
  });

});

// Recovery password FAKE 
router.post('/recoverypassword', function(req, res, next) {
  
  var user = users.find( user => (user.email === req.body.email) );
  
  if(!user){
    res.status(400);
    res.json({
      message:"No existe el correo registrado en el sistema"
    });
  }

  res.json({
    message:"Se enviara un correo con la contraseña (no sirve XD)"
  });

});

module.exports = router;
